from django.db import models

from common import domain_models

# Create your models here.
class KirkAccount(models.Model):

    owner = models.ForeignKey("auth.User", null=True, blank=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    content = models.BinaryField(null=True, blank=True)

domain_models.KirkAccount = KirkAccount
