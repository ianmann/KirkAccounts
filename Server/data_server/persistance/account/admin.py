from django.contrib import admin

from .models import KirkAccount

admin.site.register([KirkAccount])
