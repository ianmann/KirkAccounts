from business_logic.business_repositories.account_business_repositories import AccountBusinessRepository
from business_logic.domain.account_domain import AccountDomain
from business_logic.engines.engines import Engine
from common import domain_models

class AccountEngine(Engine):

    def __init__(self, engine_finder, domain=None, biz_repo=None):
        domain = domain if domain else AccountDomain()
        biz_repo = biz_repo if biz_repo else AccountBusinessRepository()
        super(AccountEngine, self).__init__(engine_finder, domain, biz_repo)

    def set_account(self, account_view_model):
        """
        Gets the account for the given user under the given account name. If the
        account doesn't exist, it is created. The content of the resulting
        account object is then set to the content in the viewmodel.
        """
        account, created = self.business_repository.data.get_or_create(
                                            owner = account_view_model.owner,
                                            name = account_view_model.name)
        account.content = account_view_model.content
        account.save()
        return account
