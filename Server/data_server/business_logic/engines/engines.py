class Engine:
    """
    Contains business flow of logic for different actions relating to a specific
    domain model.

    Fields:
        _engine_finder (required)
            Object containing all other engines in the application for use by
            this engine.

        domain (required)
            Specific implementation of certain actions relating to the domain
            model.

        business_repository (not requred)
            Abstraction for interfacing with the data and service layers.
    """

    def __init__(self, engine_finder, domain, business_repository):
        """
        Creates a new instance of an Engine for a specific domain model.
        """
        self._engine_finder = engine_finder
        self.domain = domain
        self.business_repository = business_repository

class Finder:
    """
    Contains instances for all engines in this application.
    """

    def __init__(self, account_engine=None):
        from . import account_engines
        self.ACCOUNT = account_engine if account_engine else account_engines.AccountEngine(self)
