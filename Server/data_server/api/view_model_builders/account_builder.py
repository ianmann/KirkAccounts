from .view_model_builders import Builder
from viewmodels.account import KirkAccountViewModel

class AccountViewModelBuilder(Builder):

    model = KirkAccountViewModel
