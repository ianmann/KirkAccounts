class Builder:

    def __init__(self):
        pass

    def build(self, **kwargs):
        view_model = self.model(**kwargs)
        return view_model
