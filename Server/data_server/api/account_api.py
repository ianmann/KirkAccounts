from business_logic.engines import engines
from .view_model_builders.account_builder import AccountViewModelBuilder

class AccountApi:

    def __init__(self, engine_finder=None, view_model_builder=None):
        self._engine_finder = engine_finder if engine_finder else engines.Finder()
        self.account_view_model_builder = view_model_builder if view_model_builder else AccountViewModelBuilder()

    def set_account(self, user, name, content):
        """
        Sets the account details on an account for the given user under the
        given name.
        """
        account_view_model = self.account_view_model_builder.build(owner=user, name=name, content=content)
        account_domain_model = self._engine_finder.ACCOUNT.set_account(account_view_model)
        return account_domain_model
